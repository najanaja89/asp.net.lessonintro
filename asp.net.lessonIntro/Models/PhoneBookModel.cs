namespace asp.net.lessonIntro.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PhoneBookModel : DbContext
    {
        public PhoneBookModel()
            : base("name=PhoneBookModel")
        {
        }
        public virtual DbSet<PhoneBook> PhoneBook { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
